#include <Arduino.h>
#include "servo_config.h"

#include <Wire.h>

#include <Adafruit_Sensor.h>

#include <Adafruit_PWMServoDriver.h>
#include <Adafruit_BNO055.h>

void initialize_driver(Adafruit_PWMServoDriver& target_driver, const Servo_PWM_Configuration& servo_config) 
{
  target_driver.begin();
  
  // Use the pwm_frequency from servo_config
  target_driver.setPWMFreq(servo_config.pwm_frequency);
  // Shouldn't need to change
  target_driver.setOscillatorFrequency(27000000);
}

void set_servo_to_angle(Adafruit_PWMServoDriver& target_driver, const Servo_PWM_Configuration& servo_config, uint8_t driver_channel, uint8_t desired_angle, uint8_t after_command_delay = 0) 
{
  uint16_t pulseLength = map(desired_angle, 0, 180, servo_config.servo_min, servo_config.servo_max);
  
  // Set the PWM pulse
  target_driver.setPWM(driver_channel, 0, pulseLength);
  
  if (after_command_delay != 0) 
  {
    delay(after_command_delay);
  }
}

void IMU_print_to_serial_callback(Adafruit_BNO055& imu, sensors_event_t& imu_event)
{
  imu.getEvent(&imu_event);
  
  Serial.print("X: ");
  Serial.print(imu_event.orientation.x, 2);
  Serial.print("\tY: ");
  Serial.print(imu_event.orientation.y, 2);
  Serial.print("\tZ: ");
  Serial.print(imu_event.orientation.z, 2);
  Serial.println("");
}


void kick_leg(Adafruit_PWMServoDriver& driver, const Servo_PWM_Configuration& servo_config, uint8_t driver_channel) {
  set_servo_to_angle(driver, servo_config, driver_channel, 120);
  delay(500);
  set_servo_to_angle(driver, servo_config, driver_channel, 90);
  delay(500);
}

void wave_arm(Adafruit_PWMServoDriver& driver, const Servo_PWM_Configuration& servo_config, uint8_t driver_channel) {
  for (int angle = 90; angle <= 120; angle += 15) {
    set_servo_to_angle(driver, servo_config, driver_channel, angle);
    delay(200);
  }
  for (int angle = 120; angle >= 60; angle -= 15) {
    set_servo_to_angle(driver, servo_config, driver_channel, angle);
    delay(200);
  }
}

int main(void)
{
  Wire.begin(); // SCL-19, SDA-18
  Wire1.begin(); // SDA-17, SCL-16
  Serial.begin(9600);

  // Setup Servo Drivers
  uint8_t number_of_channels = 16;
  
  const Servo_PWM_Configuration Futaba_S3305_PWM_Profile = {
    .servo_min = 100, // Minimum pulse length for 0 degrees
    .servo_max = 500, // Maximum pulse length for 180 degrees
    .us_min = 600, // Minimum pulse length in microseconds
    .us_max = 2400, // Maximum pulse length in microseconds
    .pwm_frequency = 50 // PWM frequency for the servo
  };
  Adafruit_PWMServoDriver front_driver = Adafruit_PWMServoDriver(0x40, Wire);
  initialize_driver(front_driver, Futaba_S3305_PWM_Profile);
  
  Adafruit_PWMServoDriver rear_driver = Adafruit_PWMServoDriver(0x41, Wire1);
  initialize_driver(rear_driver, Futaba_S3305_PWM_Profile);


  // Setup IMUs
  Adafruit_BNO055 imu_hand_01 = Adafruit_BNO055(0x29, &Wire);
  imu_hand_01.begin();
  imu_hand_01.setExtCrystalUse(true);
  sensors_event_t event_imu_hand_01; 

  Adafruit_BNO055 imu_foot_01 = Adafruit_BNO055(0x28, &Wire);
  imu_foot_01.begin();
  imu_foot_01.setExtCrystalUse(true);
  sensors_event_t event_imu_foot_01; 

  Adafruit_BNO055 imu_hand_02 = Adafruit_BNO055(0x29, &Wire1);
  imu_hand_02.begin();
  imu_hand_02.setExtCrystalUse(true);
  sensors_event_t event_imu_hand_02;
  
  Adafruit_BNO055 imu_foot_02 = Adafruit_BNO055(0x28, &Wire1);
  imu_foot_02.begin();
  imu_foot_02.setExtCrystalUse(true);
  sensors_event_t event_imu_foot_02;

  // Init to T pose
    for (uint8_t i = 0; i < (number_of_channels - 1); i++)
    {
      set_servo_to_angle(front_driver, Futaba_S3305_PWM_Profile, i, 90);
      Serial.print("Publishing to Front channel ");
      Serial.println(i);
      delay(250);
    }

    for (uint8_t i = 0; i < (number_of_channels - 1); i++)
    {
      set_servo_to_angle(rear_driver, Futaba_S3305_PWM_Profile, i, 90);
      Serial.print("Publishing to Rear channel ");
      Serial.println(i);
      delay(250);
    }

    delay(1000);


 
  // Main Loop
  for(;;)
  {
    wave_arm(front_driver, Futaba_S3305_PWM_Profile, 5);
    delay(500);
    wave_arm(rear_driver, Futaba_S3305_PWM_Profile, 3);
    delay(500);

    kick_leg(front_driver, Futaba_S3305_PWM_Profile, 3);
    delay(500);
    kick_leg(rear_driver, Futaba_S3305_PWM_Profile, 2);
    delay(500);

  
    IMU_print_to_serial_callback(imu_hand_01, event_imu_hand_01);
    IMU_print_to_serial_callback(imu_foot_01, event_imu_foot_01);
    IMU_print_to_serial_callback(imu_hand_02, event_imu_hand_02);
    IMU_print_to_serial_callback(imu_foot_02, event_imu_foot_02);
  }
  return 0;
}
